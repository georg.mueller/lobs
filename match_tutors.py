#!/usr/bin/env python3

# This script matches a set of tutors to a set of tutorials, i.e., it solves a shift scheduling problem.
# Each tutor has a predetermined positive number of tutorials to work. 
# Also there is a positive bias factor scaling the relative importance of his/her preferences being met.
# The tutorials are modeled as their respective time slots with the corresponding multiplicity.
# The total number of tutorials including multiplicity is assumed to be at least as great as the total capacity of the tutors and a subset of tutorials will be selected.

# The data is read and formatted from an ods file that can be generated adding minor additional information to a direct export of an appointment meeting poll from the DFN Terminplaner (https://terminplaner6.dfn.de/en)

# See the structure in example_data.ods
import os
import sys
from get_match_data import *
from task_matching import *


# set string that is prepended to output status messages for the script
preString = "  >>"

# get input argument
try: 
	datafile = sys.argv[1]
except:
	sys.exit(preString + 'You need to supply the path to an ods data file in the command line. usage: match_tutors example_data.ods')

# read the people and slot data from the data file
people, slots = get_match_data(datafile)

slotMultiplicities = [slot["multiplicity"] for slot in slots]
capacities = [person["capacity"] for person in people]
peoplesAvailabilities = [person["availability"] for person in people]
maxSlotMatches = [min(slot["availability"], slot["multiplicity"]) for slot in slots]

totalSlotMultiplicity = sum(slotMultiplicities)
totalCapacity = sum(capacities)
totalAvailability = sum(peoplesAvailabilities)
totalMaxSlotMatches = sum(maxSlotMatches)

activeSlots = [slot["multiplicity"] for slot in slots if slot["multiplicity"] != 0]

# Perform sanity check and output information
print('------------------------------------ Metadata and sanity check ------------------------------------')
print(preString + f'There are {len(people)} people available.')
print(preString + f'Total capacity is {totalCapacity}. (Number of slots that the people have to work.) \n')
print(preString + f'There are {len(slots)} disjoint time slots in the file and {len(activeSlots)} of those need to be worked.')
print(preString + f'There are {totalSlotMultiplicity} slots including multiplicity that need to be worked.\n')
print(preString + f'Total availability is {totalAvailability}. (Sum of peoplesAvailabilities over people and slots in)\n')

# Issue warning is more slots are available than capacity is
if(totalCapacity < totalSlotMultiplicity):
	print(preString + 'Total capacity does not match total slots including multiplicity. Not all slots can be worked to full multiplicity.')

# Perform sanity checks
insuffSlotMultiplicity = totalCapacity > totalSlotMultiplicity
insuffAvailability = any([1 if person['availability']<person['capacity'] else 0 for person in people])
insuffMaxSlotMatches = totalCapacity > totalMaxSlotMatches


if(insuffSlotMultiplicity or insuffAvailability or insuffMaxSlotMatches):
	print(preString + 'Your data is inconsistent and your problem will be infeasible. Listing issues:')

	if(insuffSlotMultiplicity):
		print(preString + 'Total slots including multiplicity are less than the capacity. You need more time slots for your people.')
	if(insuffAvailability):
		print(preString + 'There is a person that has indicated availability for less slots than their prescribed capacity. Listing people:')
		for person in people:
			if(person["availability"]<person["capacity"]):
				print(f'{person["name"]}: Capacity {person["capacity"]}, availability {person["availability"]}')
	if(insuffMaxSlotMatches):
		print(preString + 'Upper bound of possible matches is less than total capacity.')
		if(totalCapacity == totalSlotMultiplicity):
			print(preString + 'Your problem data shows you need every slot to be worked to full multiplicity. Listing slots where availability is less than multiplicity.')
			for slot in slots:
				if(slot["multiplicity"] > slot["availability"]):
					print(f'{slot["day"]} {slot["starttime"]}-{slot["endtime"]}: Multiplicity {slot["multiplicity"]}, availability {slot["availability"]}')
	sys.exit(preString + 'Aborting.')

# match people to slots
matches, totalHappiness, people, slots = match_tasks(people, slots, preString)

# compute maximum possible happiness
for person in people:
	person['maxHappiness'] = sum(sorted(person['preferences'],reverse=True)[:person['capacity']])
	person['relHappiness'] = person['happiness']/person['maxHappiness']
	
maxPossibleHappiness = sum([p['maxHappiness'] for p in people])
relHappiness = totalHappiness/maxPossibleHappiness

# present the results:
print('------------------------------------ Results ------------------------------------')
print(preString + f'Total happiness achieved: {int(totalHappiness)} ({relHappiness*100:.1f}% of desirable {maxPossibleHappiness})\n')

print(preString + 'Happiness of the people involved:')
for person in people:
	print(preString + f'{person["name"]}: {int(person["happiness"])} ({person["relHappiness"]*100:.1f}% of desirable {person["maxHappiness"]})')

print('\n' + preString + 'Results per person:')
for person in people:
	slotString = ', '.join([slots[i]['day']+' '+slots[i]['starttime']+'-'+slots[i]['endtime'] for i in person['matchedTasks']])
	print(f'{person["name"]}: {slotString}')
	
print('\n' + preString + 'Results per slot:')
for slot in slots:
	peopleString = ', '.join([people[i]['name'] for i in slot['matchedPeople']])
	print(f'{slot["day"]} {slot["starttime"]}-{slot["endtime"]}: {peopleString}')
