# This module implements index selection rules for the primal simplex method

import numpy as np

def blands_entering_rule(reduced_cost, tol):
	"""
	Accepts:
		       reduced_cost: the reduced cost vector of full length
		                tol: a tolerance on when to consider a reduced cost "actually" negative
	Returns: 
		       The lowest index with corresponding negative reduced cost entry
	"""
	return np.where(reduced_cost < -np.abs(tol))[0][0]

def blands_leaving_rule(restrictions):
	"""
	Accepts:
		       restrictions: the indices corresponding to the halfspaces restricting further movement along the update direction. These components will be zero in the iterate after the update was performed.
	Returns: 
		       The lowest index that restricts further movement
	"""
	return restrictions[0]

def steepest_descent_entering_rule(reduced_cost, tol):
	"""
	Accepts:
		       reduced_cost: the reduced cost vector of full length
		                tol: unused
	Returns: 
		       The index with lowest corresponding negative reduced cost entry
	"""
	return np.where(reduced_cost == min(reduced_cost))[0][0]

def largest_index_leaving_rule(restrictions):
	
	"""
	Accepts:
		       restrictions: the indices corresponding to the halfspaces restricting further movement along the update direction. These components will be zero in the iterate after the update was performed.
	Returns: 
		       The largest index that restricts further movement
	"""
	return restrictions[-1]
