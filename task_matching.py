import numpy as np
import sys
from simplex_method import *

def match_tasks(people, tasks, preString = "  >>", simplexParameters = {"max_iterations" : 1000,
"tol" : 1e-15, "verbosity" : "quiet", "keep_history" : False}):
	"""
	Matches a set of people to a set of tasks in an optimal fashion with respect to the people's preferences by using a simplex method to find an optimal vertex of a linear optimization problem whose system matrix is an (amended) incidence matrix of a bipartite graph (people and tasks are vertices, availability of a person for a task is an edge) and therefore totally unimodular, leading to integer solutions in the vertices, i.e., allocation of people to tasks.
	
	Accepts:
		           people: a list of people dictionaries, each with the following keys
		                      ["capacity"]: the number of tasks this person is supposed to be matched to
		                          ["bias"]: a scaling parameter for this person's contribution to the overall objective function.
		                   ["preferences"]: a list of numbers the length of the tasks indicating preference of this person for each of the tasks. The value 0 is interpreted as inavailability for the task
		                   ["availability"]: number of nonzero entries in the preferences entry
		              
		            tasks: a list of task dictionaries, each with the following keys
		                   ["multiplicity"]: a positive whole number indicating how many people can be matched at most with this task

		        preString: String to be printed before output
		simplexParameters: optional dictionary for simplex parameters

	Returns:
		       matches: list of person/task index tuples that were matched
		totalHappiness: the total happiness of all people
		        people: the people list, each have the added keys
		                ["happiness"]: this persons happiness
		                ["matchedTasks"]: list of tasks that were matched to this person
		         tasks: the task list, each with the added key
		                ["matchedpeople"]: list of people that were matched to this task
	# TODO update
	"""

	# Get graph metadata
	nEdges = sum([person["availability"] for person in people])
	nPeople = len(people)
	nTasks = len(tasks)
	nVertices = nPeople + nTasks  
	
	# Create containers for linear programming data
	A = np.zeros([nVertices,nEdges])
	b = np.zeros(nVertices)
	c = -np.array([val*p['bias'] for p in people for val in p['preferences'] if val!=0])
	c_unbiased = -np.array([val for p in people for val in p['preferences'] if val!=0])
	
	# Fill containers with corresponding information
	for i, person in enumerate(people):
		# determine edge index range for that person
		edgeIndexStart = sum([p['availability'] for p in people[:i]])
		edgeIndexEnd = edgeIndexStart+person['availability']
		person['edgeIndexStart'] = edgeIndexStart
		person['edgeIndexEnd'] = edgeIndexEnd
		
		# set edge start indicators in incidence Matrix
		A[i,edgeIndexStart:edgeIndexEnd] = -1

		# set persons capacity
		b[i] = -person['capacity']
		
		for personsEdge in range(0, person['availability']):
			tasksIndex = np.where(person['preferences'])[0][personsEdge]
			tasksVertexIndex = nPeople + tasksIndex
			
			# set edge end indicators for that person
			A[tasksVertexIndex, edgeIndexStart + personsEdge] = 1
			
			# set task multiplicity constraint
			b[tasksVertexIndex] = tasks[tasksIndex]['multiplicity']

	# add the inequality constraint of x <= 1 using slack variables
	A = np.vstack([np.hstack([A, np.zeros([nVertices,nEdges])]),np.hstack(2*[np.eye(nEdges)])])
	b = np.hstack([b,np.ones(nEdges)])
	c = np.hstack([c,np.zeros(nEdges)])

	# Add slacks if the total capacity is less than the total slots including multiplicity and interpret as upper bounds
	if(-sum(b[:nPeople]) < abs(sum(b[nPeople:nPeople+nTasks]))):
		A = np.hstack([ A, np.vstack([np.zeros([nPeople,nTasks]),np.eye(nTasks),np.zeros([nEdges,nTasks])]) ])
		c = np.hstack([c,np.zeros(nTasks)])

	# Some additional output
	print(preString + f'Problem dimension: {c.size}')

	# apply simplex algorithm to obtain a solution
	simplexResult = primal_simplex_method(A, b, c, B=None, parameters = simplexParameters)
	
	# check exit flag
	if simplexResult["exitflag"] == 3:
		sys.exit(preString + "Problem was detected to be infeasible. Check your data.")

	if simplexResult["exitflag"] == 2:
		sys.exit(preString + "Simplex algorithm terminated because maximum number of iterations were performed.")
		
	if simplexResult["exitflag"] == 1:
		sys.exit(preString + "Simplex algorthm terminated because the problem was detected as unbounded.")

	# construct match pairs
	activeEdgeIndices = np.where(simplexResult['solution'][:nEdges])[0]
	matches = [tuple(np.where(A[:nVertices,idx])[0] - np.array([0,nPeople])) for idx in activeEdgeIndices ]

	# save matched tasks for each person and compute individual happiness
	for i, person in enumerate(people):
		person['happiness'] = -c_unbiased[person['edgeIndexStart']:person['edgeIndexEnd']].T@simplexResult['solution'][person['edgeIndexStart']:person['edgeIndexEnd']]

		person['matchedTasks'] = [match[1] for match in matches if match[0]==i]
	
	totalHappiness = sum([person['happiness'] for person in people])
	
	# save matched people for each task
	for i, task in enumerate(tasks):
		task['matchedPeople'] = [match[0] for match in matches if match[1]==i]
		
	#pdb.set_trace()
	return matches, totalHappiness, people, tasks
