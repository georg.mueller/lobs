from pyexcel_ods import get_data
import sys
import pdb

def get_match_data(datafile):
	"""
	Reads an input datafile of the same structure as the file "example_input.ods" and returns a list of people and slots to match.
	
	Accepts:
		       datafile: file of the same structure as the file "example_input.ods"
	Returns:
		       people: a list of people dictionaries, each with the following keys
		                      ["name"]: persons name or id
		                  ["numSlots"]: the number of slots this person is supposed to be matched to
		                      ["bias"]: a scaling parameter for this person's contribution to the overall objective function.
		               ["preferences"]: a list of tuples (a,b) where a is the id of a slot (a positive whole number) and b is a nonnegative number identifying how happy this person would be when matched with the corresponding slot where 0 is interpreted as a hard constraint indicating this person is unavailable at said slot, and larger numbers b indicating larger happiness
		              
		        slots: a list of slot dictionaries, each with the following key
		                    ["slotid"]: a positive whole number identifying the slot
		                       ["day"]: the weekday
		                 ["starttime"]: starttime
		                   ["endtime"]: endtime
		              ["multiplicity"]: a positive whole number indicating how often this slot needs to be matched
	"""
	# set string that is prepended to output status messages for the script
	preString = "  >> "

	# read the data from the file and dump all empty lines
	data = get_data(datafile)['Sheet1']
	data = [row for row in data if row != []]
	
	# get the slot times
	times = [x.split('->') for x in data[1][3:]]
	
	# get the slot days
	days = [x[0:2] for x in data[0][3:]]
	for i, day in enumerate(days):
		if day == '':
			days[i] = days[i-1]
	days = days+(len(times)-len(days))*[days[-1]]
	
	# get the slot multiplicities
	multiplicities = data[2][3:]

	# get the preferences for each person
	preferences = [[2 if (x=='Yes' or x=="Ja") else 0 if (x=='No' or x=='Nein') else 1 for x in row[3:]] for row in data[4:]]

	availabilitiesPerPerson = [[1 if p > 0 else 0 for p in pref] for pref in preferences]

	#pdb.set_trace()
	totalAvailabilitiesPerSlot = [sum([a[i] for a in availabilitiesPerPerson]) for i in range(len(times))]

	# construct slots
	slots = [{'slotid': slotid,
				 'day': day,
				 'starttime': time[0],
				 'endtime': time[1],
				 'multiplicity': multiplicity,
				 'availability': availability
		 } for slotid, (day, time, multiplicity, availability) in enumerate(zip(days,times,multiplicities,totalAvailabilitiesPerSlot))]

	# get people names
	names = [row[2] for row in data[4:]]

	# get the biases
	biases = [row[0] for row in data[4:]]

	# get the number of slots to work for each person
	capacities = [row[1] for row in data[4:]]
	


	# construct people
	people = [{'name': name, 'bias': bias, 'capacity': capacity, 'preferences': preference, 'availability': sum(x!=0 for x in preference)} for (name, bias, capacity, preference) in zip(names, biases, capacities, preferences)]
	
	return people, slots
