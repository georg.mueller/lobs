% Load the documentclass.
\documentclass{scoop-exercise}

\RequirePackage{dot2texi}
% Remove the definitions of listing and listing* from the moreverb package.
% https://tex.stackexchange.com/questions/558883/how-can-i-make-minted-in-newfloat-mode-work-with-dot2texi
\csundef{listing}\csundef{endlisting}
\csundef{listing*}\csundef{endlisting*}
% Remove the definitions of comment and comment* from the verbatim package.
\csundef{comment}\csundef{endcomment}
\csundef{comment*}\csundef{endcomment*}

% Load standard packages.
\usepackage{scoop-all}
\usepackage{scoop-teaching}
\usepackage{cleveref}
\usepackage{verbatim}
\usepackage[a4paper,
bindingoffset=3mm,
left=25mm,
right=25mm, 
top=30mm,
bottom=20mm,
footskip=.25in]{geometry}
\usepackage{changes}
\newcommand{\fpref}{f_{\text{pref}}}
\usepackage{tikz}
\usetikzlibrary{angles}
\usetikzlibrary{arrows}
\usetikzlibrary{arrows.meta}
\usetikzlibrary{calc}
\usetikzlibrary{positioning}
\usetikzlibrary{decorations.markings}
\usetikzlibrary{decorations.pathmorphing}
\usetikzlibrary{math}
\usetikzlibrary{patterns}
\usetikzlibrary{shapes}

\begin{document}
  \tikzset{
    MatchGraph/.style = {circle, minimum size = 10mm},
    PersonVertex/.style = {MatchGraph, draw = green!50!black, thick, fill = green!20},
    SlotVertex/.style = {MatchGraph, draw = red!50!black, thick, fill = red!20},
  }
  
  
  \date{14.01.2024}
  \title{LOBS \\ A Linear Optimization Based Scheduler}
  \author{Dr. Georg Müller}
  
  \maketitle
  
  This document is a brief user guide to the linear optimization based scheduling tool \href{https://gitlab.com/georg.mueller/lobs}{LOBS}.
  It discusses the problems this tool was designed to solve, the mathematical optimization background and gives an introduction on how to use the tool.
  Note that this scheduler tool was designed to optimally match tutors to tutorial time slots in a university setting and it was designed to read in data from ods files containing data that can be easily created by copy-pasting results of the \href{https://terminplaner6.dfn.de/en}{DFN polling and meeting-scheduling tool}, however, it can easily be applied for any type of matching problem that can be formulated equivalently and it can be used independently of DFN if the data is created by hand.
  
  \section{Problem description}
  This tool is designed to solve the following problem from university employee shift scheduling:
  Suppose you have a class to teach and a number of time slots per week, during which labs/tutorials need to be held.
  For each time slot you have a number of rooms, in which labs can be held simultaneously.
  You have a number of tutors that can teach these labs.
  Each tutor has to teach a given number of labs per week.
  The number of room/time instances that can be taught by the employees is at least as great as the capacity of the tutors.
  Each tutor has given his/her preferences concerning the time slots.
  How can you assign tutors to time slots so the collective happiness of the people is optimal?
  
  The outermost function of the tool works on the setting described above.
  However, the mathematical modeling and solving is formulated in the slighty more abstact setting, namely:
  Suppose you have a number of tasks to be taken care of and a number of people that can take care of the tasks.
  Each task can be taken care of by a maximum number of people.
  Each person needs to perform a given number of tasks.
  The capacity of the slots is at least as great as the mandatory workload of the people.
  Each person has rated each task with a nonnegative number to indicate their preferences, higher numbers indicating greater preference.
  A preference of $0$ is interpreted as a hard inavailability constraint.
  How can you assign people to tasks so the collective happiness of the people is optimal?
  
  \section{Mathematical modelling and background}
  The problem is modelled as an integer linear optimization problem in normal form that is generated from a graph model of the problem.
  
  We have the set $P \coloneqq \{p_1,\dots,p_{n_P}\}$ of people, the set $T \coloneqq \{t_1,\dots,t_{n_T}\}$ of tasks, the set $C \coloneqq \{c_1,\dots,c_{n_P}\}$ of the people's capacities, the set $M \coloneqq \{m_1,\dots,m_{n_T}\}$ of the tasks' multiplicities as well as a preference function $\fpref\colon P\times T$ mapping a tuple $(p,t)\in P\times T$ to the preference of person $p$ for the task $t$.
  The directional graph's vertices are the people and tasks $V \coloneqq P \cup T$.
  The edges are the ordered pairs of nonzero preference, \ie,
  \begin{equation*}
    E \coloneqq \setDef{(p,t)\in P\times T}{\fpref(p,t) \neq 0}
    .
  \end{equation*}
  We enumerate the edges as $E = (e_1,\dots,e_{n_E})$. 
  Then for the edge $e_i$ we have a variable $x_i\in\{0,1\}$ indicating whether that edge (a match of a person and a task) is activated, \ie, they are matched.
  Additionally, there are bias parameters $\{b_1,\dots,b_{n_P}\}$ for each person, which act as weights in the cost functional to favour the happiness of certain people.
  They define corresponding biases $(\bar b_1, \dots, \bar b_E)$ for the edges, depending on which person they originate from.
  
  Then the task assigment problem can be modelled as the linear optimization problem
  \begin{align*}
    \text{maximize }& 
    f^\transp x \text{ for } x \in \{0,1\}^{n_E}\\
    \text{such that }
    &A_-x = -c\\
    &A_+x \leq m\\
  \end{align*}
  where the data $f \in \R_{>0}^{n_E}$, $A_- \in \R^{(n_P)\times n_E}, A_+ \in \R^{(n_T)\times n_E}$ is as follows:
  $f = (b_1 \fpref(e_1),\dots,b_E \fpref(e_{n_E}))^\transp$ and $A_-$ is the upper half of the \href{https://en.wikipedia.org/wiki/Incidence_matrix}{incidence matrix} of the directed graph containing only $-1$ and $0$ entries and $A_+$ is the lower half, \ie, in column $j$ of $A\coloneqq \begin{bmatrix}A_-\\A_+\end{bmatrix}$, there are exactly 2 nonzero entries.
  One is $-1$ at the row index corresponding to the person that the edge originates at, the other is $1$ at the row index corresponding to the task that the edge terminates at.
  Additionally, $c = (-c_1,\dots,-c_{n_P})^\transp, m=(m_1,\dots,m_{n_T})^\transp$.
  Adding slack variables to transform the 
  
  This problem formulation's main difficulty is the binary constraint on $x$ that makes this problem combinatorial.
  When this problem is relaxed to $x\in [0,1]^{n_E}$ it becomes much easier to solve.
  The admissible set of this relaxed problem is polygonal and because $A$ is an incidence matrix of a directed graph, it is totally unimodular, which implies that the admissible set has integer vertices.
  Accordingly, any optimal vertex of the relaxed problem is optimal for the binary problem. 
  The binary and the relaxed problem are bounded, and if they are feasible, then they have a solution and therefore an optimal vertex.
  Solving the relaxed problem with a simplex method (primal simplex with Bland's rule in our case) therefore either detects infeasability or yields an optimal binary vertex.
  
  Our implementation of the simplex method works on minimizing a LP in normal form, so the relaxed problem
  \begin{align*}
    \text{maximize }& 
    f^\transp x \text{ for } x \in \R^{n_E}\\
    \text{such that }
    &A_-x = -c\\
    &A_+x \leq m\\
    & x \ge 0\\
    & x \le 1
  \end{align*} 
  needs to be transformed to the equivalent normal form in block form using slack variables, \ie,
  \begin{align*}
    \text{minimize}& 
    -(f^\transp,0,0) 
    \begin{pmatrix}
      x\\s\\\tilde s
    \end{pmatrix} 
    \text{ for } (x,s,\tilde s) \in \R^{n_E} \times \R^{n_E} \times \R^{n_T}
    \\
    \text{such that }&
    \begin{bmatrix}
      A_- & 0_{n_P\times n_E} & 0_{n_P\times n_T}
      \\
      A_+ & 0_{n_T\times n_E} & I_{n_T}
      \\ 
      I_{n_E} & I_{n_E} & 0_{n_E\times n_T}
    \end{bmatrix}
    \begin{pmatrix}
      x
      \\
      s
      \\
      \tilde s
    \end{pmatrix} 
    = 
    \begin{pmatrix}
      -c
      \\
      m
      \\
      1
    \end{pmatrix}\\
    & 
    x,s,\tilde s \ge 0
    .
  \end{align*} 
  Note that if the total number of slots including multiplicity matches the capacity of all people, then the problem is equivalent to where the system is stated with $A_-x = m$, saving a few slack variables ($\tilde s$).
  
  This problem also has integer vertices (the augmented system matrix only consists of an incidence matrix and identity blocks).
  
  If the problem is solvable, then the found solution is guaranteed to be optimal.
  
  \subsection{An example}
  \label{sec:example}
  Suppose you have three people and three time slots to teach.
  During the time slots, there are one, two and one classes to teach simultaneously.
  Person A ($p_1$) has one class to teach each week and has indicated slot 2 as preferred, slot 1 as non-preferred and inavailability for slot 3.
  Person B ($p_2$) has two classes to teach each week and has indicated slots 1 and 3 as preferred and slot 2 as non-preferred.
  Person C ($p_3$) has one class to teach each week and has indicated slot 3 as preferred as well as slot 2 as non-preferred and inavailability for slot 1.
  This yields the following graph representation
  \begin{center}
    \begin{tikzpicture}[scale=2]
      \begin{dot2tex}[dot, mathmode, codeonly, tikzedgelabels]
        digraph G {
          node [style = "MatchGraph"];
          edge [lblstyle = "above,sloped"];
          PA [style = "PersonVertex", texlbl = "\small $p_1$ ($c_1 = 1$)"];
          PB [style = "PersonVertex", texlbl = "\small $p_2$ ($c_2 = 2$)"];
          PC [style = "PersonVertex", texlbl = "\small $p_3$ ($c_3 = 1$)"];
          SA [style = "SlotVertex", texlbl = "\small $t_1$ ($m_1=1$)"];
          SB [style = "SlotVertex", texlbl = "\small $t_2$ ($m_2=2$)"];
          SC [style = "SlotVertex", texlbl = "\small $t_3$ ($m_3=1$)"];
          PA -> SA [label = " ", texlbl = " $x_1, \fpref=1$ "];
          PA -> SB [label = " ", texlbl = " $x_2, \fpref=2$ "];
          PB -> SA [label = " ", texlbl = " $x_3, \fpref=2$ "];
          PB -> SB [label = " ", texlbl = " $x_4, \fpref=1$ "];
          PB -> SC [label = " ", texlbl = " $x_5, \fpref=2$ "];
          PC -> SB [label = " ", texlbl = " $x_6, \fpref=1$ "];
          PC -> SC [label = " ", texlbl = " $x_7, \fpref=2$ "];
        }
      \end{dot2tex}
    \end{tikzpicture}
  \end{center}
  and (if all biases are set to 1) the corresponding optimization problem without slack variables
  \begin{align*}
    \text{minimize }& 
    (1,2,2,1,2,1,2) 
    \begin{pmatrix}
      x_1\\x_2\\x_3\\x_4\\x_5\\x_6\\x_7
    \end{pmatrix} 
    \text{ for } x \in \R^{7}\\
    \text{such that }&
    \begin{bmatrix}
      -1 & -1 & 0 & 0 & 0 & 0 & 0\\
      0  &  0 &-1 &-1 & -1 & 0 & 0\\
      0  &  0 & 0 & 0 & 0 & -1 & -1\\
      1  &  0 & 1 & 0 & 0 & 0 & 0\\
      0  &  1 & 0 & 1 & 0 & 1 & 0\\
      0  &  0 & 0 & 0 & 1 & 0 & 1\\
    \end{bmatrix}
    \begin{pmatrix}
      x_1\\x_2\\x_3\\x_4\\x_5\\x_6\\x_7
    \end{pmatrix} 
    = 
    \begin{pmatrix}
      -1\\-2\\-1\\1\\2\\1
    \end{pmatrix}
    \\
    & x \ge 0\\
    & x \le 1
    .
  \end{align*} 
  If the bias for Person B is set to 2, then the cost function changes to 
  \begin{equation*}
    (1,2,\alert{4},\alert{2},\alert{4},1,2) 
    \begin{pmatrix}
      x_1\\x_2\\x_3\\x_4\\x_5\\x_6\\x_7
    \end{pmatrix} 
  \end{equation*}
  Both bias configurations of this example will be revisited and solved in \cref{sec:usage}.
  
  \section{System requirements}
  The tool essentially requires a working python3 implementation with only few additional packages provided.
  The requirements are very low and problems after installation should be easy to solve, so I will not go into more detail here.
  I run a linux system, but you'll likely be able to get it to work on your favourite system.
  
  \section{Tool usage}
  \label{sec:usage}
  \subsection{Obtaining and running the tool}
  You can obtain the tool using the following guideline (commands for linux):
  \begin{enumerate}
  	\item 
    Create a directory in which you want to place the tool's folder and change into this directory.
    
    \item 
    Clone the gitlab directory using \verb+git clone https://gitlab.com/georg.mueller/lobs.git+.
    
    \item 
    Change into the automatically created directory using \verb+cd lobs+.
  \end{enumerate}
  You should now be able to apply the tool to correctly formatted input files.
  There are some example files in the directory \verb+exampleInputs+.
  To make sure the tool is in working order, try solving the problem described in \cref{sec:example} by calling
\verb+./match_tutors.py exampleInputs/exampleData1.ods+.
  Your ouput should be something resembling the following:
\begin{verbatim}
YourSystemLocation> ./match_tutors.py exampleInputs/exampleData1.ods 
------------------------------------ Metadata and sanity check ------------------------------------
  >>There are 3 people available.
  >>Total capacity is 4. (Number of slots that the people have to work.) 

  >>There are 3 disjoint time slots in the file and 3 of those need to be worked.
  >>There are 4 slots that need to be worked including multiplicity.

  >>Total availability is 7. (Number of possible matches indicated)

  >>Problem dimension: 14
------------------------------------ Results ------------------------------------
  >>Total happiness achieved: 7 (87.5% of desirable 8)

  >>Happiness of the people involved:
  >>Person A: 2 (100.0% of desirable 2)
  >>Person B: 3 (75.0% of desirable 4)
  >>Person C: 2 (100.0% of desirable 2)

  >>Results per person:
Person A: Mi 14:00-16:00
Person B: Mi 11:00-13:00, Mi 14:00-16:00
Person C: Do 11:00-13:00

  >>Results per slot:
Mi 11:00-13:00: Person B
Mi 14:00-16:00: Person A, Person B
Do 11:00-13:00: Person C
\end{verbatim}
  If this checks out, you are ready to go.
  You can also try solving the remaining two examples or start providing your own data, as described in the following subsections.

  \subsection{Interpretation of the output}
  After running the tool with your input file, there will be some metadata of the problem returned, namely:
  \begin{enumerate}
   \item The number of people available.
   \item The total capacity of the people, \ie, the sum of their capacities.
   \item The total availability, \ie, the total number of possible person-slot matches -- this coincides with the number of problem variables.
   \item The number of time slots (tasks).
   \item The total slots to fill including multiplicity, \ie, the sum of the multiplicities.
   \item The result of the optimization (absolute and relative) in terms of \enquote{happiness}, \ie, the sum of the preferences of each active match of the solution (of the maximization problem) for the absolut value and the quotient of the achieved sum and the desirable value, which is determined as the sum of all best possible matches for each person (this is typically not achievable). 
   For each person $p_i$, this is the sum of the $c_i$ largest preference values of their respective possible matches. 
   \item The happiness results per person.
   \item The matches per person.
   \item The matches per time slot.
  \end{enumerate}

  \subsection{Structure of the input files}
  The input files are ods files (editable e.g.~via libreoffice calc).
  They need to have the structure shown in \cref{fig:exampleInput1}:
  \begin{figure}[h]
    \label{fig:exampleInput1}
    \centering
    \includegraphics[width=0.8\textwidth]{exampleInput1.png}
    \caption{Screenshot of the data in exampleInput1.ods}
  \end{figure}
  Gray cells need to be filled \enquote{by hand} whereas the white cells can be created copy-pasting info from DFN polls.
  It is important, that the corresponding information is at the correct location in the table, \ie, that the first person be listed in row 5 and the time slots have the format seen above.
  No is interpreted as a hard inavailability constraint, Yes ist interpreted as twice the happiness of Maybe.
  
  \subsection{Creating input files using DFN}
  You can easily transfer data from DFN polls to ods files of the required structure. Simply use the file \verb+exampleInputs/template.ods+ and:
  \begin{enumerate}
   \item 
    Create a meeting poll at \href{https://terminplaner6.dfn.de/en/node/add/meetingpoll}{DFN} and have people add their preferences for the time slots.
    
    \item Copy the first two lines with the dates by marking them as below and hitting Ctrl+C (note that the first empty field above the number of participants is marked as well)
    
    {
      \centering
      \includegraphics[width=0.8\textwidth]{DFN1.png}
    }
    
    \item Paste the result into the table at cell D1, remove the first empty cell (D1) and the one stating the participants (D2) (shift cells left), then remove the cell grouping of the days that have multiple time slots.
    
    \item Add the multiplicities to the time slots.
    
    \item Copy the lines with the peoples preferences as below and copy them by hitting Ctrl+C
    
    {
      \centering
      \includegraphics[width=0.8\textwidth]{DFN2.png}
    }
    
    \item 
    Paste the result into the table at cell C5.
    
    \item
    Run the code as before.
  \end{enumerate}

  \subsection{Use of the bias parameters}
  If all tutors are created equally, just set all bias parameters to the same function. 
  Otherwise (if some of them are close to submitting a thesis or whatever reason you deem relevant), you can increase some of them to larger values.
  For example, in \cref{sec:example}, the solution left Person B not fully satisfied. Increasing the corresponding bias from 1 to 2 yields the following result:
  \begin{verbatim}
YourSystemLocation> ./match_tutors.py ./exampleInputs/exampleData1.ods 
------------------------------------ Metadata and sanity check ------------------------------------
  >>There are 3 people available.
  >>Total capacity is 4. (Number of slots that the people have to work.) 

  >>There are 3 disjoint time slots in the file and 3 of those need to be worked.
  >>There are 4 slots that need to be worked including multiplicity.

  >>Total availability is 7. (Number of possible matches indicated)

  >>Problem dimension: 14
------------------------------------ Results ------------------------------------
  >>Total happiness achieved: 7 (87.5% of desirable 8)

  >>Happiness of the people involved:
  >>Person A: 2 (100.0% of desirable 2)
  >>Person B: 4 (100.0% of desirable 4)
  >>Person C: 1 (50.0% of desirable 2)

  >>Results per person:
Person A: Mi 14:00-16:00
Person B: Mi 11:00-13:00, Do 11:00-13:00
Person C: Mi 14:00-16:00

  >>Results per slot:
Mi 11:00-13:00: Person B
Mi 14:00-16:00: Person A, Person C
Do 11:00-13:00: Person B

  \end{verbatim}
  \ie a solution where Person C is not fully happy but Person B now is.
  Note that the stated happiness in the results is always unbiased, you don't see the actual cost functional value of the underlying model.
  Also note that in this example, the overall happiness is still $7$, as in the unbiased case, but this is not true in general -- biasing certain people may decrease the overall happiness.
\end{document}
