# This module implements the primal simplex method for solving linear programs
# It consists of two functions: The "actual" simplex method in run_primal_simplex_method,
# which expects a basis for a initial feasible basic vector as input, and the wrapper
# primal_simplex_method, which will do a phase I if no initial basis was provided for
# the original problem.

import numpy as np
from index_selection_rules import *

def primal_simplex_method(A, b, c, B = None, entering_rule = blands_entering_rule, leaving_rule = blands_leaving_rule, parameters={}):
	"""
	Solve a linear optimization problem in standard form, i.e., a problem of
	the form
		min c*x 
		s.t. Ax = b, x >= 0,
	using the primal simplex method with Bland's rule.
	
	This function wraps the actual simplex in order to include a phase-I-search, if 
	no initial feasible basic point is provided.
	
	Accepts:
		            c: the linear cost's vector
		            A: the constraint's matrix
		            b: the constraint's right-hand-side vector
		            B: basis indices for initial feasible basic vector
		               defaults to None, in which case a basis will be determined from a Phase I
		entering_rule: the rule that selects the index that enters the basis from the reduced cost vector
		 leaving_rule: the rule that selects the index that leaves the basis from the quotient vector
		   parameters: optional parameters (dictionary);
		               the following key/value pairs are evaluated:
		               ["max_iterations"]: maximum number of iterations
		                          ["tol"]: tolerance for optimality check on reduced cost vector entries and feasibility in phase I
		                    ["verbosity"]: "verbose" or "quiet"
		                 ["keep_history"]: whether or not to store the iteration history (0, 1 or 2)

	Returns:
		result: a dictionary containing the key/value pairs
		            solution: final iterate
		            function: the final iterate's objective value
		       basis_indices: the final iterate's basis indices
		          iterations: number of iterations performed
		            exitflag: flag encoding why the algorithm terminated
		                      0: optimal solution found
		                      1: problem is unbounded
		                      2: maximum number of iterations reached
		                      3: problem is infeasible (In which case all other fields of result will be None)  
	"""
	# Check some parameter requirements and set to default if not provided
	verbosity = parameters.get("verbosity", "quiet")
	tol = parameters.get("tol", 0)
	
	# If initial basis for feasible basic vector was not provided, find one in phase I
	if B is None:
		# Dump some output
		if verbosity == 'verbose':
			print("No initial basis provided. Starting phase I.")
		
		# Get problem dimensions
		m, n = A.shape
		
		# Generate input data for the phase-I-problem
		A_I = np.hstack((A, np.diag(np.where(b >= 0, 1, -1))))
		b_I = b
		c_I = np.hstack((np.zeros(n), np.ones(m)))
		B_I = range(n, n+m)

		# Solve phase-I-problem
		phase_I_result = run_primal_simplex_method(A_I, b_I, c_I, B_I, entering_rule, leaving_rule, parameters)
		
		# Check if phase I terminated correctly
		if phase_I_result["exitflag"] == 2:
			raise BaseException("Phase I terminated because the maximum number of iterations were performed.")
		
		if phase_I_result["exitflag"] == 1:
			raise BaseException("Phase I terminated because the problem was detected as unbounded.")
		
		# Check for feasibility of the initial problem
		if any(phase_I_result["solution"][n:] > np.abs(tol)):
			result = {
				"solution" : None,
				"function" : None,
				"basis_indices" : None,
				"iterations" : None,
				"exitflag" : 3
			}
			
			# Dump some output
			if verbosity == 'verbose':
				print('\n\nOriginal problem was found to be infeasible in phase I. Phase II won\'t be performed.\n')
				
			return result
		
		# Get initial basis for phase II from solution of phase I
		B = list(set(phase_I_result["basis_indices"]))
		N_real = list(set(range(n)) - set(B))

		# Remove all unwanted artificial indices from basis in case of degeneracy (postprocessing)
		try:
			# Get greatest basis index
			max_basis_index = max(B)
			
			# Dump some output
			if verbosity == 'verbose' and max_basis_index >= n:
				print('\n\nPhase I basis contains artificial indices. Starting post-processing.\n')
			
			# If greatest basis index is artificial, swap it out or remove redundancy in the system and dump the index
			while max_basis_index >= n:  
				# Obtain representations for nonbasic columns of A in current basis
				D = np.linalg.solve( A_I[:,B], A[:,N_real] )
				
				# Determine relative position of max index in B
				rel_max_basis_index = np.where(np.array(B, copy=False)  == max_basis_index)[0][0]
				
				# swap-case
				if(D[rel_max_basis_index,:].any()):
					# Determine swap index as the greatest of the ones contributing most to e_max_basis_index (guaranteed to be nonzero)
					entering_basis = N_real[np.argmax(np.abs(D[rel_max_basis_index,:]))]

					# Swap out greatest index in B
					B = list(set(B) - set([max_basis_index]) | set([entering_basis]))
					
					if verbosity == 'verbose':
						print("Replaced artificial basis index %4d with real index %4d." % (max_basis_index, entering_basis))
				
				# dump-case
				else:
					# dump artificial index from basis
					B = list(set(B) - set([max_basis_index]))
					
					rel_artificial_index = max_basis_index - n

					# remove corresponding constraint from phase I data
					A_I = np.vstack([A_I[:rel_artificial_index,:],A_I[rel_artificial_index+1:,:]])
					
					# remove corresponding constraint from phase II data
					A = np.vstack([A[:rel_artificial_index,:],A[rel_artificial_index+1:,:]])
					b = np.hstack([b[:rel_artificial_index],b[rel_artificial_index+1:]])
					m = m-1
				
					if verbosity == 'verbose':
							print("Removed index %4d from basis and removed the redundant line %4d from the equality constraints." % (max_basis_index, rel_artificial_index))
				
				# Get new greatest basis index
				max_basis_index = max(B)
				
				# Update nonbasis
				N_real = list(set(range(n)) - set(B))
				
		except:
			raise ValueError("Post-processing Phase-I basis failed.") 
			
	else: # B was not None
		# Dump some output
		if verbosity == 'verbose':
			print("Initial basis provided.")
	
	# Run the phase II optimization
	if verbosity == 'verbose':
		print("\nStarting Optimization.")

	result = run_primal_simplex_method(A, b, c, B, entering_rule, leaving_rule, parameters)

	return result
	
def run_primal_simplex_method(A, b, c, B, entering_rule, leaving_rule, parameters):
	"""
	Solve a linear optimization problem in standard form, i.e., a problem of
	the form
		min c*x 
		s.t. Ax = b, x >= 0,
	using the primal simplex method with Bland's rule.
	Expects a Basis for an initial feasible basic point to be supplied.

	Accepts:
		            c: the linear cost's vector
		            A: the constraint's matrix
		            b: the constraint's right-hand-side vector
		            B: basis indices for initial feasible basic vector
		   parameters: optional parameters (dictionary);
		entering_rule: the rule that selects the index that enters the basis from the reduced cost vector
		 leaving_rule: the rule that selects the index that leaves the basis from the quotient vector
		   parameters: optional parameters (dictionary);
		               the following key/value pairs are evaluated:
		               ["max_iterations"]: maximum number of iterations
		                          ["tol"]: tolerance for optimality check on reduced cost vector entries and feasibility in phase I
		                    ["verbosity"]: "verbose" or "quiet"
		                 ["keep_history"]: whether or not to store the iteration history (True or False)

	Returns:
		       result: a dictionary containing the key/value pairs
		                    solution: final iterate
		                    function: the final iterate's objective value
		               basis_indices: the final iterate's basis indices
		                  iterations: number of iterations performed
		                    exitflag: flag encoding why the algorithm terminated
		                              0: optimal solution found
		                              1: problem is unbounded
		                              2: maximum number of iterations reached
	"""

	def print_header(): 
		print('----------------------------------------------------------------------------------------------')
		print(' ITER          OBJ      OBJCHNG   MINREDCOST   CHSREDCOST     ENTERING      LEAVING           ')
		print('----------------------------------------------------------------------------------------------')

	# Get the algorithmic parameters, using defaults if missing
	max_iterations = parameters.get("max_iterations", 1e3)
	tol = parameters.get("tol", 0)
	verbosity = parameters.get("verbosity", "quiet")
	keep_history = parameters.get("keep_history", False)

	# Define exitflags that will be printed when the algorithm terminates
	exitflag_messages = [
		'Reached optimal solution.',		
		'Problem is unbounded.',
		'Reached maximum number of optimization steps.',
	]

	# Get problem dimensions
	m, n = A.shape

	# Remove all duplicate indices from initial basis and check length
	B = list(set(B))
	if len(B) != m:
		raise ValueError("Size of initial basis does not match problem dimension")

	# Initialize initial iterate and non-basis index set from initial basis
	x = np.zeros(n)
	try:
		x[B] = np.linalg.solve(A[:,B], b)
	except: 
		raise ValueError("Could not compute iterate from basis.")
	N = list(set(list(range(n))) - set(B))
	
	# Check the initial iterate for feasibility
	if any(x < 0): 
		raise ValueError("Initial iterate is infeasible.")

	# Initialize variables for simplex iteration loop
	iterations = 0
	exitflag = None
	f_old = np.inf

	# Prepare a dictionary to store the history
	if keep_history:
		history = {
			"iterates" : [],
			"objective_values" : [],
			"basis_indices" : [],
			"entering_basis" : [],
			"leaving_basis" : []
		}

	# Perform simplex iterations until termination criterion is met
	while exitflag is None:

		# Compute current function value 
		f = c.T @ x

		# Record the current iterate, its function value and the basis
		if keep_history: 
			history["iterates"].append(x.copy())
			history["objective_values"].append(f)
			history["basis_indices"].append(B)

		# Dump some output
		if verbosity == 'verbose':
			if (iterations%10 == 0): print_header()
			print(' %4d  %11.4e  %11.4e' % (iterations, f, f-f_old), end = '')

		# Stop when the maximum number of iterations has been reached
		if iterations >= max_iterations:
			exitflag = 2
			break

		try:
			# Compute reduced cost vector (sN)
			reduced_cost = np.zeros(n)
			lmbda = np.linalg.solve(A[:,B].T, c[B])
			reduced_cost[N] = c[N] - A[:,N].T @ lmbda
		except:
			raise ValueError("Reduced cost vector could not be computed.")

		# Dump some output
		if verbosity == 'verbose': 
			print('  %11.4e' % (np.amin(reduced_cost)), end = '')

		# Check for optimality by checking sign of reduced cost vector entries
		# and also check if numerical inaccuracy is prohibiting proper termination 
		if all(reduced_cost >= -np.abs(tol)):
			exitflag = 0
			break

		# Remember function value
		f_old = f

		# Select new entering basis index using Bland's rule
		entering_basis = entering_rule(reduced_cost, tol)
		
		# Dump some output
		if verbosity == 'verbose': 
			print('  %11.4e' % (reduced_cost[entering_basis]), end = '')

		# Record the index that enters_basis 
		if keep_history: history["entering_basis"].append(entering_basis)

		# Dump some output
		if verbosity == 'verbose': print('         %4d  ' % (entering_basis), end = '')

		# Compute update direction
		delta_x = np.zeros(n)
		try:
			delta_x[B] = - np.linalg.solve(A[:,B], A[:,entering_basis])
		except:
			raise ValueError("Update direction could not be computed.")

		# Check for unboundedness
		if all(delta_x >= -np.abs(tol)):
			exitflag = 1
			break

		# Compute steplength t from quotient test
		quotients = np.full(n, np.inf)
		quotients[delta_x < -np.abs(tol)] = -x[delta_x < -np.abs(tol)] / delta_x[delta_x < -np.abs(tol)]
		step_length = np.amin(quotients)
		
		# Compute index that will leave the basis
		leaving_basis = leaving_rule(np.where(quotients == step_length)[0])

		# Record the leaving basis index
		if keep_history: history["leaving_basis"].append(leaving_basis)

		# Dump some output
		if verbosity == 'verbose': print('       %4d  ' % (leaving_basis))
		
		# Update iterate
		x[B] = x[B] + step_length * delta_x[B]
		x[leaving_basis] = 0
		x[entering_basis] = step_length

		# Update basis and nonbasis
		B = list(set(B) - set([leaving_basis]) | set([entering_basis]))
		N = list(set(N) - set([entering_basis]) | set([leaving_basis]))

		iterations = iterations + 1 

	# Dump some output
	if verbosity == 'verbose':
		print('\n\nThe simplex method is exiting with flag %d.\n' %(exitflag) + str(exitflag_messages[exitflag])+'\n' )

	# Create and populate the result to be returned
	result = {
		"solution" : x,
		"dual_solution" : lmbda,
		"dual_slack": reduced_cost,
		"function" : c.T @ x,
		"basis_indices" : B,
		"iterations" : iterations,
		"exitflag" : exitflag
	}

	# Assign the history to the result if required
	if keep_history: result["history"] = history

	return result
